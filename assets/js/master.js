$(document).ready(function () {
    function removeAllSidebarToggleClass() {
        $('#toggle-on').removeClass('d-md-block d-none');
        $('#toggle-off').removeClass('d-md-none');
    }



    $('#toggle-on').click(function () {
        $('#aside').fadeOut(300);
        $('#main-body').animate({
            width: '100%'
        }, 300);

        setTimeout(() => {
            removeAllSidebarToggleClass();
            $('#toggle-on').addClass('d-none');
            $('#toggle-off').removeClass('d-none');
        }, 300);

    });

    $('#toggle-off').click(function () {
        $('#aside').fadeIn(300);
        setTimeout(() => {
            removeAllSidebarToggleClass();
            $('#toggle-off').addClass('d-none');
            $('#toggle-on').removeClass('d-none');
        }, 300);

    });

    $('#menu-toggle').click(function () {
        $('#body-header').toggle(300);
    })


    $('#search-toggle').click(function () {
        $('#search-toggle').addClass('d-none');
        $('#search-area').removeClass('d-none');
        $('#search-input').animate({ width: '12rem' }, 300);
    })

    $('#search-area-hide').click(function () {
        $('#search-input').animate({ width: '0rem' }, 300);
        setTimeout(() => {
            $('#search-area').addClass('d-none');
            $('#search-toggle').removeClass('d-none');
        }, 300);
    })

    $('#header-notification-toggle').click(function(){
        $('#header-notification').fadeToggle(300);
    })

    $('#comment-toggle').click(function(){
        $('#header-comment').fadeToggle(300);
    })

    $('#header-profile-toggle').click(function(){
        $('#header-profile').fadeToggle(300);
    })

    $('.sidebar-group-link').click(function(){
        $('.sidebar-group-link').removeClass('sidebar-group-link-active');
        $('.sidebar-group-link').children('.sidebar-dropdown-toggle').children('.angle').removeClass('icon-chevron-down');
        $('.sidebar-group-link').children('.sidebar-dropdown-toggle').children('.angle').addClass('icon-chevron-left');
        $(this).addClass('sidebar-group-link-active');
        $(this).children('.sidebar-dropdown-toggle').children('.angle').removeClass('icon-chevron-left');
        $(this).children('.sidebar-dropdown-toggle').children('.angle').addClass('icon-chevron-down');
     });


     $('#full-screen').click(function(){
        toggleFullScreen();
     });
  
     function toggleFullScreen()
     {
        if((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)){
           if(document.documentElement.requestFullscreen){
              document.documentElement.requestFullscreen();
           }
           else if(document.documentElement.mozRequestFullscreen){
              document.documentElement.mozRequestFullscreen();
           }
           else if(document.documentElement.webkitRequestFullscreen){
              document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
           }
           $('#screen-compress').removeClass('d-none');
           $('#screen-expand').addClass('d-none');
        }
        else{
           if(document.cancelFullScreen)
           {
              document.cancelFullScreen();
           }
           else if(document.mozCancelFullScreen)
           {
              document.mozCancelFullScreen();
           }
           else if(document.webkitCancelFullScreen)
           {
              document.webkitCancelFullScreen();
           }
           $('#screen-compress').addClass('d-none');
           $('#screen-expand').removeClass('d-none');
        }
     }
});


